# -*- coding: utf-8 -*-

##################################################
## Author: Daniel Freire
## Email: danielbfr3@gmail.com
##################################################

from selenium.webdriver.common.keys import Keys
from selenium import webdriver, common
import click, json, sys


def load_json(filename):
    """Open json file and return a python dictionary of it."""

    try:
        with open(file=filename) as json_data:
            return json.load(json_data)
    except FileNotFoundError:
        print('File not found.')

def write_json(data):
    """Pick a python dictionary and save it as a json file."""
    
    try:
        with open(file='output.json', mode='w', encoding='utf-8') as json_file:

            formatted_file = json.dumps(data, indent=4, sort_keys=True)

            print(formatted_file)
            json_file.write(formatted_file)
            return True
    except:
        print('Error while saving.')

def extract_topics(data):
    """Extract search topics from a python dictonary."""
    
    try:
        return [ x for x in data['google-me'] ]
    except:
        return None

@click.group()
def main():
    """>>>NextelBot<<< CLI for parsing json files and searching for each topic within. Made by Daniel Freire."""
    pass

@main.command()
@click.argument('filename')
def search(filename):
    """Loads a json file and search for the first 3 google results."""

    try:
        json_file = load_json(filename)
        search_topics = extract_topics(json_file)
        output = {'output': {}}

        # creates a selenium object
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('window-size=1280x800')
        browser = webdriver.Chrome(options=options)

        for topic in search_topics:
            browser.get('https://www.google.com.br/')
            browser.find_element_by_xpath('//*[@id="lst-ib"]').send_keys(topic + Keys.ENTER)

            blocks_of_links = browser.find_elements_by_class_name('bkWMgd') #blocks of links

            result = {}

            for links in blocks_of_links:
                for result_data in links.find_elements_by_css_selector('div > div > div > div.r > a'):
                    if len(result) == 3 or result_data.text == '':
                        break
                    else:
                        title = result_data.text.split('\n')[0]
                        link = result_data.get_attribute("href")

                        if 'https://translate.google.com.br/translate' in link:
                            continue
                        else:
                            result[len(result)] = {title : link}

            output['output'][topic] = result

        browser.quit()
        write_json(output)


    except KeyboardInterrupt:
        browser.quit()
        print('Program is terminating.')

    except common.exceptions.NoSuchElementException:
        browser.quit()
        print('Program is terminating for connection problem.')

    except common.exceptions.TimeoutException:
        browser.quit()
        print('Program is terminating because the timeout time is expired.')


if __name__ == "__main__":
    main()

